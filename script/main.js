// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript 
// Async operations in Java script are those which are essential for managing tasks that might take time to complete,
//  such as fetching data from a server, reading/writing files, or any other I/O operations. JavaScript's 
//  single-threaded nature means that blocking operations can halt the execution of other tasks, leading to a poor user experience.
//  Asynchronous programming allows you to execute multiple tasks concurrently without blocking the main thread.



// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
const button = document.querySelector(".container__button");
const container = document.querySelector(".container");

async function buttonAjaxFetch() {
    await fetch("https://api.ipify.org/?format=json", {
            method: "GET"
        })
        .then((response) => {
            return response.json();
        })
        .then( async (data) =>  {
            const ip = data.ip;
            console.log(ip);
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.               
            const ipInfoResponse = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,region,city,district,regionName`)
    
            .then( (response) => {
                return response.json();
            })
            .then((dataInfo) => {
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.  
                console.log(dataInfo);
                const continent = dataInfo.continent;
                const country = dataInfo.country;
                const region = dataInfo.region;
                const city = dataInfo.city;
                const regionName = dataInfo.regionName;
                const divBelowButton = document.createElement("div");
                container.appendChild(divBelowButton);
                divBelowButton.classList.add("ipInfo");
                divBelowButton.innerHTML = `Continent:${continent}; Country:${country}; Region:${region}; City:${city}; RegionName:${regionName};`;
            })
        });    
}

button.addEventListener("click", buttonAjaxFetch, { once: true });
